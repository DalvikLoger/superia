export const config = {
    api: {
        bodyParser: false, // Disables body parsing, you'll handle it manually
    },
};